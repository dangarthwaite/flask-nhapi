NH General Court 3rd party REST API
===================================

Overlays the public access MS SQL database with a thin REST API.

Building and developing in docker
------------------------------

    # Just build the container
    make build

    # Drop you into a root shell
    make shell

    # Not implemented yet
    make run

Tentative API endpoints
-----------------------

    /<year>
        /house
            # list of committee name & ids
        /senate
            # list of committee name & ids

        /<body>
            /bills
                # topical bills, +/- 6 days?
            /bills?q='some text'
                # list of bills, by chronological distance
            /bills/<bills_id>
                # bill stuff, sponsors, votes, docket

            /legislators?q='some text'
                # list of legislators
            /legislators/<legislators_id>
                # Returns person
            /legislators/votes
                # Recent vots, +/- 6 days

            /committees
                # list of committee_ids
            /committees/<committees_id>
                # all member info + all docket +/- 6 days

            /votes
                # recent votes, +/- 6 days
            /votes/<votes_id>
                # roll call

