.PHONY: run shell build test

run: build
	docker run --rm -it dang/nhapi:wip /app/venv/bin/python /usr/sbin/supervisord

test: build
	docker run -e HOME=/root -e TERM=xterm-color -e SHELL=bash --name=nhapi-wip --hostname=nhapi --rm -it dang/nhapi:wip /tmp/venv/bin/python /app/models.py

shell: build
	docker run -e HOME=/root -e TERM=xterm-color -e SHELL=bash --name=nhapi-wip --hostname=nhapi --rm -it dang/nhapi:wip bash

build: Dockerfile app
	docker build -t 'dang/nhapi:wip' .
