from flask import Flask, Response, jsonify

app = Flask(__name__)


@app.route('/')
def index():
    return Response("test", 200)


@app.route('/<year>/<house>/bills/<int:id>')
@app.route('/<year>/<house>/bills/<string:billno>')
def bill_id(year, house, id=False, billno=False):
    bill = {}
    bill['billno'] = "HB409"
    bill['sponsors'] = ['12323']
    bill['title'] = "Some good idea for the greater good"
    bill['text'] = ("Take from folks that will always vote, "
                    "give it to swing voters.")
    return jsonify(bill)


@app.route('/<int:year>/<house>/legislators/<int:id>')
def legislator(year, house, id=False):
    person = dict(first='John', last='Doe', id=12323)
    return jsonify(person)


@app.route('/<int:year>/<house>/votes/<int:id>')
def votes(year, house, id=False):
    roll_call = {}
    roll_call['votes'] = ((12322, -1), (123123, 1), (12330, -1), (12312, 0))
    roll_call['delta'] = sum([v for k, v in roll_call['votes']])
    roll_call['yeas'] = len([v for k, v in roll_call['votes'] if v == 1])
    roll_call['nays'] = len([v for k, v in roll_call['votes'] if v == -1])
    roll_call['pct'] = roll_call['sum'] * 1.0 / len(roll_call['votes'])
    return jsonify(roll_call)


@app.route('/<int:year>/<house>/committees/<int:id>')
def committees(year, house, id=False):
    cmte = {"Labor": 123, "Commerce": 131}
    return jsonify(cmte)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
