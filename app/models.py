#!/usr/bin/env python
from database import Base
from sqlalchemy import Column, Integer, String, Boolean, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref


class County(Base):
    __tablename__ = 'tblCounty'
    CountyCode = Column('CountyCode', String, primary_key=True)
    CountyDescription = Column('CountyDescription', String)
    CountyAbbreviation = Column('CountyAbbreviation', String)
    Legislators = relationship('Legislators', backref='County', lazy='dynamic')
    def __repr__(self):
        return '<County {CountyCode} {CountyAbbreviation}>'.format(**self.__dict__)


class Legislators(Base):
    __tablename__ = 'VLegislators'
    EmployeeNo = Column('EmployeeNo', String, primary_key=True)
    LastName = Column('LastName', String)
    FirstName = Column('FirstName', String)
    MiddleName = Column('MiddleName', String)
    Active = Column('Active', Integer)
    LegislativeBody = Column('LegislativeBody', String)
    SeatNo = Column('SeatNo', String)
    CountyCode = Column('CountyCode', String, ForeignKey(County.CountyCode))
    District = Column('District', String)
    Party = Column('Party', String)
    Sex = Column('Sex', String)
    Street = Column('Street', String)
    Address2 = Column('Address2', String)
    City = Column('City', String)
    State = Column('State', String)
    ZipCode = Column('ZipCode', String)
    EMailAddress1 = Column('EMailAddress1', String)
    def __repr__(self):
        return '<Legislators {EmployeeNo}: {LastName}, {FirstName}>'.format(**self.__dict__)


class Sponsors(Base):
    __tablename__ = 'VLSRSponsors'
    SessionYear = Column('SessionYear', Integer, primary_key=True)
    LSR = Column('LSR', Integer, primary_key=True)
    EmployeeNo = Column('EmployeeNo', String, ForeignKey(Legislators.EmployeeNo))
    PrimeSponsor = Column('PrimeSponsor', Boolean)
    def __repr__(self):
        return '<Sponsors {SessionYear} {LSR}, {EmployeeNo}>'.format(**self.__dict__)


class RollCallSummary(Base):
    __tablename__ = 'VRollCallSummary'
    VoteDate = Column('VoteDate', String, primary_key=True)
    SessionYear = Column('SessionYear', Integer, primary_key=True)
    LegislativeBody = Column('LegislativeBody', String, primary_key=True)
    VoteSequenceNumber = Column('VoteSequenceNumber', Integer, primary_key=True)
    RollCallDate = Column('RollCallDate', String)
    CondensedBillNo = Column('CondensedBillNo', String)
    Yeas = Column('Yeas', Integer)
    Nays = Column('Nays', Integer)
    Present = Column('Present', Integer)
    AbbreviatedTitle2 = Column('AbbreviatedTitle2', String)
    AbbreviatedTitle1 = Column('AbbreviatedTitle1', String)
    Absent = Column('Absent', Integer)
    Question_Motion = Column('Question_Motion', String)
    Title1 = Column('Title1', String)
    Title2 = Column('Title2', String)
    Expr1 = Column('Expr1', DateTime)
    def __repr__(self):
        return '<RollCallSummary {SessionYear} {VoteDate} {LegislativeBody} {VoteSequenceNumber} {Question_Motion}>'.format(**self.__dict__)


class RollCallHistory(Base):
    __tablename__ = 'tblRollCallHistory'
    EmployeeNumber = Column('EmployeeNumber', String, ForeignKey(Legislators.EmployeeNo), primary_key=True)
    SessionYear = Column('SessionYear', Integer, primary_key=True)
    LegislativeBody = Column('LegislativeBody', String, primary_key=True)
    VoteSequenceNumber = Column('VoteSequenceNumber', Integer, primary_key=True)
    CondensedBillNo = Column('CondensedBillNo', String)
    Vote = Column('Vote', Integer)
    def __repr__(self):
        return '<RollCallHistory {EmployeeNumber} {SessionYear} {LegislativeBody} {VoteSequenceNumber} {CondensedBillNo} {Vote}>'.format(**self.__dict__)


class Secretaries(Base):
    __tablename__ = 'tblSecretaries'
    SecretaryID = Column('SecretaryID', Integer, primary_key=True)
    LastName = Column('LastName', String)
    FirstName = Column('FirstName', String)
    EmailAddress = Column('EmailAddress', String)
    def __repr__(self):
        return '<Secretaries {SecretaryID} {LastName}, {FirstName}>'.format(**self.__dict__)


class SenateDistricts(Base):
    __tablename__ = 'tblSenateDistricts'
    CountyCode = Column('CountyCode', String, ForeignKey('tblCounty.CountyCode'))
    County = Column('County', String)
    DistrictCode = Column('districtcode', String)
    Town = Column('Town', String)
    Ward = Column('Ward', Integer)
    counter = Column('counter', Integer, primary_key=True)
    def __repr__(self):
        #return '<SenateDistrict {counter} {County} {DistrictCode}>'.format(**self.__dict__)
        return '<SenateDistrict {counter} {County}>'.format(**self.__dict__)


class Senators(Base):
    __tablename__ = 'tblSenators'
    PKey = Column('PKey', Integer, primary_key=True)
    EmployeeNo = Column('EmployeeNo', String, ForeignKey('VLegislators.EmployeeNo'))
    Building = Column('Building', String)
    Room = Column('Room', String)
    Phone = Column('phone', String)
    def __repr__(self):
        return '<Senator {EmployeeNo} {Building} {Room} {Phone}>'.format(**self.__dict__)


class StatusCodes(Base):
    __tablename__ = 'tblStatusCodes'
    StatusCode = Column('StatusCode', String, primary_key=True)
    StatusDescription = Column('StatusDescription', String)
    def __repr__(self):
        return '<StatusCodes {StatusCode} {StatusDescription}>'.format(**self.__dict__)
        #return '<StatusCodes {StatusCode}>'.format(**self.__dict__)

class SubjectCodes(Base):
    __tablename__ = 'tblSubjectCodes'
    SubjectCode = Column('SubjectCode', String, primary_key=True)
    SubjectDescription = Column('SubjectDescription', String)
    def __repr__(self):
        return '<SubjectCode {SubjectCode} {SubjectDescription}>'.format(**self.__dict__)

class BodyStatusCodes(Base):
    __tablename__ = 'tblBodyStatusCodes'
    BodyStatusCode = Column('BodyStatusCode', String, primary_key=True)
    StatusDescription = Column('StatusDescription', String)
    def __repr__(self):
        return '<BodyStatusCode {BodyStatusCode} {StatusDescription}>'.format(**self.__dict__)

class CommitteeMembers(Base):
    __tablename__ = 'tblCommitteeMembers'
    CommitteeCode = Column('CommitteeCode', String, primary_key=True)
    SequenceNumber = Column('SequenceNumber', Integer)
    EmployeeNumber = Column('EmployeeNumber', String)
    ActiveMember = Column('ActiveMember', Boolean)
    Comments = Column('Comments', String)
    def __repr__(self):
        return '<ComitteeCode {CommitteeCode} {SequenceNumber} {EmployeeNumber} {ActiveMember} {Comments}>'.format(**self.__dict__)

class CommitteeReportCodes(Base):
    __tablename__ = 'tblCommitteeReportCodes'
    CommitteeReportCode = Column('CommitteeReportCode', String, primary_key=True)
    CommitteeReportDescription = Column('CommitteeReportDescription', String)
    CommitteeReportAbbreviation = Column('CommitteeReportAbbreviation', String)
    def __repr__(self):
        #return '<ComitteeReportCode {CommitteeReportCode} {CommitteeReportAbbreviation} >'.format(**self.__dict__)
        return '<ComitteeReportCode {CommitteeReportCode}>'.format(**self.__dict__)

class Committees(Base):
    __tablename__ = 'tblCommittees'
    CommitteeCode = Column('CommitteeCode', String, primary_key=True)
    CommitteeName = Column('CommitteeName', String)
    CommitteeAbbreviation = Column('CommitteeAbbreviation', String)
    CommitteeLocation = Column('CommitteeLocation', String)
    CommitteePhone = Column('CommitteePhone', String)
    CommitteeSecretary = Column('CommitteeSecretary', String)
    ActiveCommittee = Column('ActiveCommittee', Boolean)
    CommitteeResearcher = Column('CommitteeResearcher', String)
    CommitteeMeetingSchedule = Column('CommitteeMeetingSchedule', String)
    CommitteeEmailAddress = Column('CommitteeEmailAddress', String)
    def __repr__(self):
        return '<Comittee {CommitteeCode} {CommitteeAbbreviation} {ActiveCommittee} {CommitteeEmailAddress}>'.format(**self.__dict__)


class HouseDistricts(Base):
    __tablename__ = 'tblhousedistricts'
    counter = Column('counter', Integer, primary_key=True)
    countycode = Column('countycode', String) # nvarchar
    County = Column('County', String)
    Districtcode = Column('Districtcode', String) #nvarchar
    Ward = Column('Ward', Integer)
    Town = Column('Town', String)
    def __repr__(self):
        return '<HouseDistrict {counter} {Town} {Ward}>'.format(**self.__dict__)

class Dockets(Base):
    __tablename__ = 'tblDocket'
    SessionYear = Column('SessionYear', Integer, primary_key=True)
    LSR = Column('LSR', Integer, primary_key=True)
    ExpandedBillNo = Column('ExpandedBillNo', String)
    StatusDate = Column('StatusDate', DateTime)
    CondensedBillNo = Column('CondensedBillNo', String)
    LegislativeBody = Column('LegislativeBody', String)
    Description = Column('Description', String)
    def __repr__(self):
        return '<Docket {SessionYear} {LSR} {CondensedBillNo}>'.format(**self.__dict__)


class GeneralStatusCodes(Base):
    __tablename__ = 'tblGeneralStatusCodes'
    GeneralCode = Column('GeneralCode', String, primary_key=True)
    GeneralDescription = Column('GeneralDescription', String)
    GeneralStatusActiveCode = Column('GeneralStatusActiveCode', Boolean)
    def __repr__(self):
        return '<GeneralStatusCode {GeneralCode} {GeneralDescription} {GeneralStatusActiveCode}>'.format(**self.__dict__)


class LSRs(Base):
    __tablename__ = 'tblLSRs'
    SessionYear = Column('SessionYear', Integer, primary_key=True)
    LSR = Column('LSR', Integer, primary_key=True)
    LSRTitle = Column('LSRTitle', String)
    DateLSREntered = Column('DateLSREntered', DateTime)
    LegislativeBody = Column('LegislativeBody', String)
    BillType = Column('BillType', Integer)
    AppropriationCode = Column('AppropriationCode', Boolean)
    FiscalImpactCode = Column('FiscalImpactCode', Boolean)
    LocalCode = Column('LocalCode', Boolean)
    FullLSR = Column('FullLSR', String)
    SubjectCode = Column('SubjectCode', String)
    ExpandedBillNo = Column('ExpandedBillNo', String)
    CondensedBillNo = Column('CondensedBillNo', String)
    DateLSRBill = Column('DateLSRBill', DateTime)
    ChapterNo = Column('ChapterNo', String)
    HouseCommitteeReferralCode = Column('HouseCommitteeReferralCode', String)
    HouseCurrentCommitteeCode = Column('HouseCurrentCommitteeCode', String)
    HouseDateIntroduced = Column('HouseDateIntroduced', DateTime)
    HouseStatusCode = Column('HouseStatusCode', String)
    HouseStatusDate = Column('HouseStatusDate', DateTime)
    HouseDueDate = Column('HouseDueDate', DateTime)
    HouseFloorDate = Column('HouseFloorDate', DateTime)
    HouseAmended = Column('HouseAmended', Boolean)
    SenateCommitteeReferralCode = Column('SenateCommitteeReferralCode', String)
    SenateCurrentCommitteeCode = Column('SenateCurrentCommitteeCode', String)
    SenateDateIntroduced = Column('SenateDateIntroduced', DateTime)
    SenateStatusCode = Column('SenateStatusCode', String)
    SenateStatusDate = Column('SenateStatusDate', DateTime)
    SenateDueDate = Column('SenateDueDate', DateTime)
    SenateFloorDate = Column('SenateFloorDate', DateTime)
    SenateAmended = Column('SenateAmended', Boolean)
    GeneralStatusCode = Column('GeneralStatusCode', String)
    GeneralStatusDate = Column('GeneralStatusDate', DateTime)
    Rereferred = Column('Rereferred', Boolean)
    CurrentLSRStatus = Column('CurrentLSRStatus', String)
    LatestCommitteeHearingCode = Column('LatestCommitteeHearingCode', String)
    LatestCommitteeHearingDate = Column('LatestCommitteeHearingDate', DateTime)
    LatestCommitteeHearingPlace = Column('LatestCommitteeHearingPlace', String)
    LatestReportingCommitteeCode = Column('LatestReportingCommitteeCode', String)
    LatestMajorityReportCode = Column('LatestMajorityReportCode', String)
    LatestMinorityReportCode = Column('LatestMinorityReportCode', String)
    Retained = Column('Retained', Boolean)

    def __repr__(self):
        return '<LSR {SessionYear} {LSR} {CondensedBillNo} {LSRTitle}>'.format(**self.__dict__)


class LSRStatus(Base):
    __tablename__ = 'tblLSRStatus'
    SessionYear = Column('SessionYear', Integer, primary_key=True)
    LSR = Column('LSR', Integer, primary_key=True)
    LSRSequenceNo = Column('LSRSequenceNo', Integer, primary_key=True)
    StatusCode = Column('StatusCode', String)
    def __repr__(self):
        return '<LSRStatus {SessionYear} {LSR} {LSRSequenceNo} {StatusCode}>'.format(**self.__dict__)

if __name__ == "__main__":
    from pprint import pprint
    for klass in (
#        Legislators,
#        Sponsors,
#        RollCallSummary,
#        RollCallHistory,
        Secretaries,
        SenateDistricts,
#        Senators,
#        StatusCodes,
#        SubjectCodes,
#        BodyStatusCodes,
#        CommitteeMembers,
#        CommitteeReportCodes,
#        Committees,
#        County,
#        HouseDistricts,
#        Dockets,
#        GeneralStatusCodes,
#        LSRStatus,
#        LSRs,
            ):
        pprint(klass.query.first())
    tammy = Legislators.query.filter_by(FirstName='Tammy', LastName='Simmons').first()
    print "Tammy in Hillsborough: %s" % (tammy.County.CountyAbbreviation == 'Hills')
    print "Tammy's votes on 2012 SB348: "
    votes = RollCallHistory.query.filter_by(
        EmployeeNumber=tammy.EmployeeNo,
        SessionYear=2012,
        CondensedBillNo='SB348').all()
    for vote in votes:
	print "\t{0}".format(vote)
