FROM ubuntu:14.04
MAINTAINER Dan Garthwaite <dan@garthwaite.org>

RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
        build-essential \
        git \
        python \
        python-dev \
        python-virtualenv \
        nginx \
        supervisor

RUN service supervisor stop && service nginx stop

RUN apt-get build-dep -y \
        python-pyodbc \
        python-gevent

RUN apt-get install -y \
        unixodbc \
        tdsodbc \
        freetds-dev \
        vim

COPY ./ops/nginx.conf /etc/nginx/nginx.conf
COPY ./ops/supervisor.conf /etc/supervisor/conf.d/app.conf
COPY ./ops/freetds.conf /etc/freetds/freetds.conf
COPY ./ops/odbc.ini /etc/odbc.ini
COPY ./ops/odbcinst.ini /etc/odbcinst.ini
      
COPY ./requirements.txt /tmp/requirements.txt

RUN virtualenv /tmp/venv
RUN /tmp/venv/bin/pip install -r /tmp/requirements.txt

ADD ./app /app

CMD ["/usr/bin/supervisord"]
EXPOSE 5000
